#include "FreeRTOS.h"
#include "task.h"

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/cm3/systick.h>



void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed portCHAR *pcTaskName);


//-----------------------------------------------------------------------------
// Link to FreeRTOS
//-----------------------------------------------------------------------------

void sv_call_handler(void);
void pend_sv_handler(void);
void sys_tick_handler(void);


void sv_call_handler(void) {
	vPortSVCHandler();
}

void pend_sv_handler(void) {
	xPortPendSVHandler();
}

void sys_tick_handler(void) {
	xPortSysTickHandler();
}


//-----------------------------------------------------------------------------
// Blink2
//-----------------------------------------------------------------------------


void vApplicationStackOverflowHook(
    xTaskHandle *pxTask __attribute((unused)),
    signed portCHAR *pcTaskName __attribute((unused))) {

    while(1) {
    }
}

void clock_setup(void) {
	rcc_clock_setup_hse_3v3(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

	systick_set_reload(168000); // 168 MHz 1 ms
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB); // source selection
	systick_counter_enable();

	systick_interrupt_enable();
}

static void gpio_setup() {
    rcc_periph_clock_enable(RCC_GPIOG);

    gpio_mode_setup(
        GPIOG,
        GPIO_MODE_OUTPUT,
        GPIO_PUPD_NONE,
        GPIO13);

    gpio_mode_setup(
        GPIOG,
        GPIO_MODE_OUTPUT,
        GPIO_PUPD_NONE,
        GPIO14);
}

static void task1(void * args __attribute((unused))) {
    while(1) {
        gpio_toggle(GPIOG, GPIO13);
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

static void task2(void * args __attribute((unused))) {
    while(1) {
        gpio_toggle(GPIOG, GPIO14);
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

int main(void) {

    clock_setup();
    gpio_setup();

    xTaskCreate(task1, "LED13", 100, NULL, configMAX_PRIORITIES - 1, NULL);
    xTaskCreate(task2, "LED14", 100, NULL, configMAX_PRIORITIES - 1, NULL);

    vTaskStartScheduler();

    while (1) {
    }

    return 0;
}


