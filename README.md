# Projects for STM32 boards 

This repo contains different projects for STM32 boards.

These projects use the GNU tools and libopencm3 and sometimes FreeRTOS.


## Blink1 (for STM32f429I-DISC1 board)
[blink1](https://gitlab.com/oscalbert/stm32/tree/master/STM32F429I/blink1) is a kind of "hello word !" project. It is a minimalist project that can serve as a basis for another projects.
It only blinks a led. It only uses libopencm3 open source library.

## Blink2 (for STM32f429I-DISC1 board)
[blink2](https://gitlab.com/oscalbert/stm32/tree/master/STM32F429I/blink2) uses FreeRTOS to make two leds blinking. Feel free to reuse it !